package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloApplicationController {
	
	@GetMapping("/hello")
	public String sayHello() {
		
		return "Hello Earthling! If you are seeing this text, that means you've been authenticated!"; 
		
	}

}
